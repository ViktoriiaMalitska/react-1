import React, { Component }  from 'react';

class NewMessageForm extends React.Component {
    constructor() {
      super()
      this.state = {
        message: ''
      }
      this.handleChange = this.handleChange.bind(this)
      this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(e) {
      this.setState({
        message: e.target.value
      })
    }

    handleSubmit(e) {
      e.preventDefault()
      this.props.sendMessage({text:this.state.message, userId: this.props.userId})
      this.setState({
        message: ''
      })
    }
    render() {
      return (
        <div
          onSubmit={this.handleSubmit}
          className="sendMessageForm">
          <input
          className="sendMessageForm "
            onChange={this.handleChange}
            value={this.state.message}
            placeholder="Message"
            type="text" />
            <button type="submit">{`Send`}</button>
        </div>
      )
    }
  }

export default NewMessageForm;