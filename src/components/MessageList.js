import React from "react";
import Message from "./Message";

const MessageList = (props) => {
  let { userId, list } = props;

  return (
    <div className="messagesContainer">
      {list.map((item) => (
        <Message
          key={item.id}
          userId={item.userId}
          avatar={item.userId === userId ? "#" : item.avatar}
          text={item.text}
          data={item.createdAt}
          id={item.id}
        />
      ))}
    </div>
  );
};

export default MessageList;
