import React, { Component } from "react";
import icon from "../svg/heart-solid.svg";
import { updateMessageFetch, removeMessageFetch } from "../api";

const Image = (props) => {
  return <img className={`${props.alt}`} src={props.src} alt={props.alt} />;
};

class Message extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      avatar: this.props.avatar,
      text: this.props.text,
      data: this.props.data,
      userId: this.props.userId,
      id: this.props.id,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.deleteMessage = this.deleteMessage.bind(this);
  }

  handleChange(event) {
    this.setState({ text: event.target.value });
  }
  async deleteMessage() {
      alert(`Message was deleted  ${this.state.id}`);
      const response = await removeMessageFetch();
  }

  async handleSubmit(event) {
    event.preventDefault();
      alert(`Message was update  ${this.state.text}`);
      const response = await updateMessageFetch({
        userId: this.state.userId,
        text: this.state.text,
      });
  }

  render() {
    const dateParse = new Date(Date.parse(this.state.data));
    const date = `${dateParse.getHours() - 3}:${dateParse.getMinutes()}`;

    if (this.state.avatar === "#") {
      return (
        <div className="messageItem rightSide">
          <input
          className="textRightSide"
            type="text"
            id={this.state.userId}
            value={this.state.text}
            onChange={this.handleChange}
          />
          <input type="submit" value="Update" onClick={this.handleSubmit} />
          <input type="submit" value="Delete" onClick={this.deleteMessage} />
          <div className="dateMessage">{date}</div>
        </div>
      );
    } else {
      return (
        <div className="messageItem leftSide">
          <Image src={`${this.state.avatar}`} alt="avatar" />
          <img src={icon} className="like" alt="like" />
          <div className="textMessage">{this.state.text}</div>
          <div className="dateMessage">{date}</div>
         
        </div>
      );
    }
  }
}
export default Message;

