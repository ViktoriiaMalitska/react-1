import React from "react";
import { CHAT_NAME } from "../config";

const Header = (props) => {
  let { listMessage } = props;
  console.log(listMessage);
  let setId = new Set();
  const totalUsers = (listMessage, setId) => {
    listMessage.forEach((element) => {
      setId.add(element.userId);
    });
    return setId;
  };
  let date;
  const usersAmount = totalUsers(listMessage, setId).size;
  const totalMessages = listMessage.length;
  let lastMessage = listMessage[totalMessages - 1];
  if (!!lastMessage) {
    lastMessage = lastMessage.createdAt;
    date = new Date(Date.parse(lastMessage));

    date = `${date.getHours() - 3}:${date.getMinutes()}`;
  } else lastMessage = null;

  if (listMessage.length > 0) {
    return (
      <div className="headerContainer">
        <div className="headeritem">{CHAT_NAME}</div>
        <div className="headeritem">{`${usersAmount}  participants`}</div>
        <div className="headeritem">{`${totalMessages}  messages`}</div>
        <div className="headeritem">{`last message at ${date}`}</div>
      </div>
    );
  }
  return null;
};

export default Header;
