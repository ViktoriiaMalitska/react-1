import React, { Component }  from 'react';
import './App.css';
import {getMessagesListFetch, sendMessageFetch } from './api';
import Header from "./components/Header.js";
import  MessageList  from "./components/MessageList";
import OutHeader from "./components/OutHeader";
import NewMessageForm from "./components/CreateMessage";
import Footer from "./components/Footer";
import preloader from "./gif/835.gif";



class Chat extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      listMessage: [],
      userId: "9e243930-83c9-11e9-8e0c-8f1a686f4ce4",
      listIsFetching: true
    }
  }
  sortListMessage = (list) => {
    list.sort((cur, next) => {
      return (new Date(cur.createdAt) - new Date(next.createdAt))
    })
  }
  handleListMessages = (list) => {
    let sortList = list.sort((cur, next) => {
      return (new Date(cur.createdAt) - new Date(next.createdAt))
    })
    this.setState({
      listMessage: sortList
    })
  }

  async getMessagesList() {
    setTimeout(() => {
      this.setState({ listIsFetching: false });
    }, 12000);
    const response = await getMessagesListFetch();
    const result = await response.json();
    this.handleListMessages(result);
  }

  async sendMessage(data) {
    const response = await sendMessageFetch(data);
    alert(`New message created  ${data.text}`)
    this.getMessagesList();
  }
  

  componentDidMount() {
    this.getMessagesList();
  }

  render () {
    return(
      <div className="mainContainer">
       
        <OutHeader/>
        <Header listMessage = {this.state.listMessage}/>
        {this.state.listIsFetching ? <image className="preloader" width="100px" src={preloader}/> : null}
        <MessageList key={this.state.userId} userId = {this.state.userId} list = {this.state.listMessage} />
        <NewMessageForm   userId = {this.state.userId} sendMessage = {data => {this.sendMessage(data)}}/>
        <Footer/>
      </div>
    )
  }

}
export default Chat;
