import { API_URL} from "./utils";

export const getMessagesListFetch = () =>
  fetch(`${API_URL}`, {
    method: "Get",
  });

export const sendMessageFetch = (data) =>
  fetch(
    `#`,
    {
      method: "Get",
    },
    JSON.stringify(data)
  );

export const updateMessageFetch = (data) =>
  fetch(
    `#`,
    {
      method: "Get",
    },
    JSON.stringify(data)
  );

export const removeMessageFetch = (id) =>
  fetch(`#${id}`, {
    method: "DELETE",
  });
